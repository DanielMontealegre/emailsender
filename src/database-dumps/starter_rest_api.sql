-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: starter_rest_api
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administradores`
--

DROP TABLE IF EXISTS `administradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `password` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_rol` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  KEY `administradores_fk1_idx` (`id_rol`),
  CONSTRAINT `administradores_fk1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores`
--

LOCK TABLES `administradores` WRITE;
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
INSERT INTO `administradores` VALUES (1,'xadmin','64179dc60bc3af46a0826d40489cfe4f83352599','Baum','Digital',1,'testing@baumtesting.com',NULL,'2015-07-01 13:22:00',1),(2,'admin','64179dc60bc3af46a0826d40489cfe4f83352599','Admin','Ejemplo',2,'email@dominio.com',NULL,'2015-07-01 13:22:00',1);
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campos`
--

DROP TABLE IF EXISTS `campos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campos` (
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campos`
--

LOCK TABLES `campos` WRITE;
/*!40000 ALTER TABLE `campos` DISABLE KEYS */;
/*!40000 ALTER TABLE `campos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `orden` int(11) DEFAULT '0',
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias_contenidos`
--

DROP TABLE IF EXISTS `categorias_contenidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias_contenidos` (
  `categoria` int(11) NOT NULL,
  `contenido` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoria`,`contenido`),
  KEY `fk2_categorias_contenidos_idx` (`contenido`),
  CONSTRAINT `fk1_categorias_contenidos` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk2_categorias_contenidos` FOREIGN KEY (`contenido`) REFERENCES `contenidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias_contenidos`
--

LOCK TABLES `categorias_contenidos` WRITE;
/*!40000 ALTER TABLE `categorias_contenidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias_contenidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_canton`
--

DROP TABLE IF EXISTS `cm_canton`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_canton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_provincia` int(11) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_canton_fk1_idx` (`id_provincia`) USING BTREE,
  CONSTRAINT `cm_canton_fk1` FOREIGN KEY (`id_provincia`) REFERENCES `cm_provincia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_canton`
--

LOCK TABLES `cm_canton` WRITE;
/*!40000 ALTER TABLE `cm_canton` DISABLE KEYS */;
INSERT INTO `cm_canton` VALUES (1,1,'San José'),(2,1,'Escazú'),(3,1,'Desamparados'),(4,1,'Puriscal'),(5,1,'Tarrazú'),(6,1,'Aserrí'),(7,1,'Mora'),(8,1,'Goicoechea'),(9,1,'Santa Ana'),(10,1,'Alajuelita'),(11,1,'Vázquez de Coronado'),(12,1,'Acosta'),(13,1,'Tibás'),(14,1,'Moravia'),(15,1,'Montes de Oca'),(16,1,'Turrubares'),(17,1,'Dota'),(18,1,'Curridabat'),(19,1,'Pérez Zeledón'),(20,1,'León Cortés Castro'),(21,2,'Alajuela'),(22,2,'San Ramón'),(23,2,'Grecia'),(24,2,'San Mateo'),(25,2,'Atenas'),(26,2,'Naranjo'),(27,2,'Palmares'),(28,2,'Poás'),(29,2,'Orotina'),(30,2,'San Carlos'),(31,2,'Zarcero'),(32,2,'Valverde Vega'),(33,2,'Upala'),(34,2,'Los Chiles'),(35,2,'Guatuso'),(36,3,'Cartago'),(37,3,'Paraíso'),(38,3,'La Unión'),(39,3,'Jiménez'),(40,3,'Turrialba'),(41,3,'Alvarado'),(42,3,'Oreamuno'),(43,3,'El Guarco'),(44,4,'Heredia'),(45,4,'Barva'),(46,4,'Santo Domingo'),(47,4,'Santa Bárbara'),(48,4,'San Rafael'),(49,4,'San Isidro'),(50,4,'Belén'),(51,4,'Flores'),(52,4,'San Pablo'),(53,4,'Sarapiquí'),(54,5,'Liberia'),(55,5,'Nicoya'),(56,5,'Santa Cruz'),(57,5,'Bagaces'),(58,5,'Carrillo'),(59,5,'Cañas'),(60,5,'Abangares'),(61,5,'Tilarán'),(62,5,'Nandayure'),(63,5,'La Cruz'),(64,5,'Hojancha'),(65,6,'Puntarenas'),(66,6,'Esparza'),(67,6,'Buenos Aires'),(68,6,'Montes de Oro'),(69,6,'Osa'),(70,6,'Quepos'),(71,6,'Golfito'),(72,6,'Coto Brus'),(73,6,'Parrita'),(74,6,'Corredores'),(75,6,'Garabito'),(76,7,'Limón'),(77,7,'Pococí'),(78,7,'Siquirres'),(79,7,'Talamanca'),(80,7,'Matina'),(81,7,'Guácimo');
/*!40000 ALTER TABLE `cm_canton` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_cliente`
--

DROP TABLE IF EXISTS `cm_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(45) DEFAULT NULL COMMENT 'Cédula o pasaporte',
  `nombre` varchar(45) DEFAULT NULL COMMENT '	',
  `apellidos` varchar(75) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `id_genero` int(11) DEFAULT NULL COMMENT '1- masculino\n2- femenino',
  `id_canton` int(11) DEFAULT NULL,
  `imagen_perfil` varchar(250) DEFAULT NULL,
  `acepta_notificaciones` tinyint(1) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL COMMENT '-1. Eliminado\n1. Activo\n2. Inactivo',
  `fb_user` tinyint(1) DEFAULT NULL,
  `fb_id` varchar(75) DEFAULT NULL,
  `id_idioma` varchar(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_cliente_fk1_idx` (`id_genero`) USING BTREE,
  KEY `cm_cliente_fk2_idx` (`id_canton`) USING BTREE,
  KEY `cm_cliente_fk3_idx` (`id_idioma`) USING BTREE,
  CONSTRAINT `cm_cliente_fk1` FOREIGN KEY (`id_genero`) REFERENCES `cm_genero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cm_cliente_fk2` FOREIGN KEY (`id_canton`) REFERENCES `cm_canton` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cm_cliente_fk3` FOREIGN KEY (`id_idioma`) REFERENCES `sys_idioma` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_cliente`
--

LOCK TABLES `cm_cliente` WRITE;
/*!40000 ALTER TABLE `cm_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_email_send_error`
--

DROP TABLE IF EXISTS `cm_email_send_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_email_send_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicio` varchar(20) DEFAULT NULL COMMENT '-mandrill, -ses_aws, -smtp',
  `detalle` text,
  `parametros` longtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ultimo_intento` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `intentos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_email_send_error`
--

LOCK TABLES `cm_email_send_error` WRITE;
/*!40000 ALTER TABLE `cm_email_send_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_email_send_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_genero`
--

DROP TABLE IF EXISTS `cm_genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_genero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_label_nombre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_genero_fk1_idx` (`id_label_nombre`) USING BTREE,
  CONSTRAINT `cm_genero_fk1` FOREIGN KEY (`id_label_nombre`) REFERENCES `sys_label` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_genero`
--

LOCK TABLES `cm_genero` WRITE;
/*!40000 ALTER TABLE `cm_genero` DISABLE KEYS */;
INSERT INTO `cm_genero` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `cm_genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_log_errores`
--

DROP TABLE IF EXISTS `cm_log_errores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_log_errores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicio` varchar(255) DEFAULT NULL,
  `controlador` varchar(255) DEFAULT NULL,
  `plataforma` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `version_so` varchar(100) DEFAULT NULL,
  `conexion` varchar(100) DEFAULT NULL,
  `request` longtext,
  `error` longtext,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_log_errores`
--

LOCK TABLES `cm_log_errores` WRITE;
/*!40000 ALTER TABLE `cm_log_errores` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_log_errores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_mensaje_push`
--

DROP TABLE IF EXISTS `cm_mensaje_push`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_mensaje_push` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_label_titulo` int(11) DEFAULT NULL,
  `id_label_mensaje` int(11) DEFAULT NULL,
  `respuesta_servicio` varchar(45) DEFAULT NULL,
  `id_mensaje_enviado` int(11) DEFAULT NULL,
  `cant_vistos` int(11) DEFAULT '0',
  `estado` tinyint(4) DEFAULT NULL COMMENT '0: inactivo\n1: activo',
  `fecha_envio` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_mensaje_push_fk1_idx` (`id_label_titulo`) USING BTREE,
  KEY `cm_mensaje_push_fk2_idx` (`id_label_mensaje`) USING BTREE,
  CONSTRAINT `cm_mensaje_push_fk1` FOREIGN KEY (`id_label_titulo`) REFERENCES `sys_label` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `cm_mensaje_push_fk2` FOREIGN KEY (`id_label_mensaje`) REFERENCES `sys_label` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_mensaje_push`
--

LOCK TABLES `cm_mensaje_push` WRITE;
/*!40000 ALTER TABLE `cm_mensaje_push` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_mensaje_push` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_mensaje_push_personal`
--

DROP TABLE IF EXISTS `cm_mensaje_push_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_mensaje_push_personal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `id_label_titulo` int(11) DEFAULT NULL,
  `id_label_mensaje` int(11) DEFAULT NULL,
  `fecha_envio` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_mensaje_push_personal_fk1_idx` (`id_cliente`) USING BTREE,
  KEY `cm_mensaje_push_personal_fk2_idx` (`id_label_titulo`) USING BTREE,
  KEY `cm_mensaje_push_personal_fk3_idx` (`id_label_mensaje`) USING BTREE,
  CONSTRAINT `cm_mensaje_push_personal_fk1` FOREIGN KEY (`id_cliente`) REFERENCES `cm_cliente` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `cm_mensaje_push_personal_fk2` FOREIGN KEY (`id_label_titulo`) REFERENCES `sys_label` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `cm_mensaje_push_personal_fk3` FOREIGN KEY (`id_label_mensaje`) REFERENCES `sys_label` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_mensaje_push_personal`
--

LOCK TABLES `cm_mensaje_push_personal` WRITE;
/*!40000 ALTER TABLE `cm_mensaje_push_personal` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_mensaje_push_personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_notification`
--

DROP TABLE IF EXISTS `cm_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `badge` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cm_notification_fk1_idx` (`id_cliente`),
  CONSTRAINT `cm_notification_fk1` FOREIGN KEY (`id_cliente`) REFERENCES `cm_cliente` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_notification`
--

LOCK TABLES `cm_notification` WRITE;
/*!40000 ALTER TABLE `cm_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_notification_info`
--

DROP TABLE IF EXISTS `cm_notification_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_notification_info` (
  `id_notification` int(11) NOT NULL,
  `endpoint` varchar(255) NOT NULL,
  `topic_subscription` varchar(255) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_notification`,`endpoint`),
  CONSTRAINT `cm_notification_info_fk1` FOREIGN KEY (`id_notification`) REFERENCES `cm_notification` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_notification_info`
--

LOCK TABLES `cm_notification_info` WRITE;
/*!40000 ALTER TABLE `cm_notification_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_notification_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_notification_info_guest`
--

DROP TABLE IF EXISTS `cm_notification_info_guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_notification_info_guest` (
  `device_id` varchar(255) NOT NULL,
  `endpoint` varchar(255) DEFAULT NULL,
  `topic_subscription` varchar(255) DEFAULT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_notification_info_guest`
--

LOCK TABLES `cm_notification_info_guest` WRITE;
/*!40000 ALTER TABLE `cm_notification_info_guest` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_notification_info_guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_pais`
--

DROP TABLE IF EXISTS `cm_pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_pais`
--

LOCK TABLES `cm_pais` WRITE;
/*!40000 ALTER TABLE `cm_pais` DISABLE KEYS */;
INSERT INTO `cm_pais` VALUES (1,'Costa Rica');
/*!40000 ALTER TABLE `cm_pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_password_token`
--

DROP TABLE IF EXISTS `cm_password_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_password_token` (
  `id_cliente` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_password_token`
--

LOCK TABLES `cm_password_token` WRITE;
/*!40000 ALTER TABLE `cm_password_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_password_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_provincia`
--

DROP TABLE IF EXISTS `cm_provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pais` int(11) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_provincia_fk1_idx` (`id_pais`) USING BTREE,
  CONSTRAINT `cm_provincia_fk1` FOREIGN KEY (`id_pais`) REFERENCES `cm_pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_provincia`
--

LOCK TABLES `cm_provincia` WRITE;
/*!40000 ALTER TABLE `cm_provincia` DISABLE KEYS */;
INSERT INTO `cm_provincia` VALUES (1,1,'San José'),(2,1,'Alajuela'),(3,1,'Cartago'),(4,1,'Heredia'),(5,1,'Guancaste'),(6,1,'Puntarenas'),(7,1,'Limón');
/*!40000 ALTER TABLE `cm_provincia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_session_token`
--

DROP TABLE IF EXISTS `cm_session_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_session_token` (
  `token` text NOT NULL,
  `secret` varchar(255) NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_session_token`
--

LOCK TABLES `cm_session_token` WRITE;
/*!40000 ALTER TABLE `cm_session_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_session_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuraciones`
--

DROP TABLE IF EXISTS `configuraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `project_safe_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `default_theme` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `default_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `online_site` tinyint(4) NOT NULL DEFAULT '1',
  `offline_cont` text COLLATE utf8_unicode_ci NOT NULL,
  `analytics_code` text COLLATE utf8_unicode_ci NOT NULL,
  `dev_mode` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuraciones`
--

LOCK TABLES `configuraciones` WRITE;
/*!40000 ALTER TABLE `configuraciones` DISABLE KEYS */;
INSERT INTO `configuraciones` VALUES (1,'Starter','starter','starter/','inicio',1,'<h1 style=\"text-align: center;\">sitio offline</h1>\n','<script></script>',1);
/*!40000 ALTER TABLE `configuraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenidos`
--

DROP TABLE IF EXISTS `contenidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` int(11) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idioma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resumen` text COLLATE utf8_unicode_ci,
  `contenido` text COLLATE utf8_unicode_ci,
  `estado` int(11) DEFAULT '1',
  `orden` int(11) DEFAULT '0',
  `last_mod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk1_contenidos_idx` (`padre`),
  KEY `fk2_contenidos_idx` (`url`),
  CONSTRAINT `fk1_contenidos` FOREIGN KEY (`padre`) REFERENCES `contenidos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk2_contenidos` FOREIGN KEY (`url`) REFERENCES `urls` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenidos`
--

LOCK TABLES `contenidos` WRITE;
/*!40000 ALTER TABLE `contenidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contenidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES (2,'contacto','Test','testing@baumtesting.com');
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenido` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1_imagenes_idx` (`contenido`),
  CONSTRAINT `fk1_imagenes` FOREIGN KEY (`contenido`) REFERENCES `contenidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenes`
--

LOCK TABLES `imagenes` WRITE;
/*!40000 ALTER TABLE `imagenes` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mandrill_error`
--

DROP TABLE IF EXISTS `mandrill_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mandrill_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `detalle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parametros` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `ultimo_intento` datetime DEFAULT NULL,
  `intentos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mandrill_error`
--

LOCK TABLES `mandrill_error` WRITE;
/*!40000 ALTER TABLE `mandrill_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `mandrill_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_admin`
--

DROP TABLE IF EXISTS `menu_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_admin`
--

LOCK TABLES `menu_admin` WRITE;
/*!40000 ALTER TABLE `menu_admin` DISABLE KEYS */;
INSERT INTO `menu_admin` VALUES (1,'menu_owner'),(2,'menu_admin');
/*!40000 ALTER TABLE `menu_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_admin_item`
--

DROP TABLE IF EXISTS `menu_admin_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_admin_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL DEFAULT '0',
  `id_padre` int(11) DEFAULT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `es_separador` tinyint(1) DEFAULT '0',
  `orden` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `menu_admin_items_fk1_idx` (`id_menu`),
  CONSTRAINT `menu_admin_item_fk1` FOREIGN KEY (`id_menu`) REFERENCES `menu_admin` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_admin_item`
--

LOCK TABLES `menu_admin_item` WRITE;
/*!40000 ALTER TABLE `menu_admin_item` DISABLE KEYS */;
INSERT INTO `menu_admin_item` VALUES (1,1,NULL,'Categorías','categorias',0,1),(2,1,NULL,'Contenidos','contenidos',0,2),(3,1,NULL,'Media','media',0,3),(4,1,NULL,'Menus','menus',0,4),(5,1,NULL,'SEO','seo',0,5),(6,1,NULL,'Registros','registros',0,6),(7,1,NULL,'sp','sp',1,7),(8,1,NULL,'Roles','roles',0,8),(9,1,NULL,'Administradores','administradores',0,9),(10,2,NULL,'Contenidos','contenidos',0,1),(11,2,NULL,'Categorías','categorias',0,2),(12,2,NULL,'Media','media',0,3),(13,2,NULL,'Menus','menus',0,4);
/*!40000 ALTER TABLE `menu_admin_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL DEFAULT '0',
  `id_padre` int(11) DEFAULT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_visible` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_li` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_i` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `orden` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk1_menu_items_idx` (`id_menu`),
  CONSTRAINT `fk1_menu_items` FOREIGN KEY (`id_menu`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idioma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo`
--

DROP TABLE IF EXISTS `modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `subtitulo` text,
  `modelo` varchar(100) DEFAULT NULL COMMENT 'Modelo que será cargado',
  `url` varchar(100) DEFAULT NULL COMMENT 'Nombre de carpeta/Valor utilizado para cargar el modulo "dominio/admin/<url>"',
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo`
--

LOCK TABLES `modulo` WRITE;
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;
INSERT INTO `modulo` VALUES (1,'Categorías','Creación, edición y manipulación de las categorías del sitio','Categorias','categorias',1),(2,'Contenidos','Creación, edición y manipulación de los contenidos del sitio','Contenidos','contenidos',1),(3,'Media','Gestión de archivos (imágenes, documentos de texto, comprimidos, etc)',NULL,'media',1),(4,'Menus','Gestión de menus del sitio','Menus','menus',1),(5,'SEO','Gestión de urls para Search Engine Optimization',NULL,'seo',1),(6,'Roles','Gestión de los roles del sitio. Se da acceso o no a los módulos y scripts','Rol','roles',1),(7,'Administradores','Gestión de administradores del sitio','Administradores','administradores',1),(8,'Configuración','Gestión de la configuración del sitio','Configuraciones','configuracion',1),(9,'Registros','Gestión de los registros del sitio','Registros','registros',1);
/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `administrador` int(11) NOT NULL DEFAULT '0',
  `token` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrador` (`administrador`),
  UNIQUE KEY `token` (`token`),
  CONSTRAINT `password_resets_ibfk_1` FOREIGN KEY (`administrador`) REFERENCES `administradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registros`
--

DROP TABLE IF EXISTS `registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_registro` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros`
--

LOCK TABLES `registros` WRITE;
/*!40000 ALTER TABLE `registros` DISABLE KEYS */;
/*!40000 ALTER TABLE `registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `id_modulo_default` int(11) NOT NULL DEFAULT '0',
  `id_menu` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rol_fk1_idx` (`id_modulo_default`),
  KEY `rol_fk2_idx` (`id_menu`),
  CONSTRAINT `rol_fk1` FOREIGN KEY (`id_modulo_default`) REFERENCES `modulo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rol_fk2` FOREIGN KEY (`id_menu`) REFERENCES `menu_admin` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Owner',2,1),(2,'Admin',2,2);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_script`
--

DROP TABLE IF EXISTS `rol_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_script` (
  `id_rol` int(11) NOT NULL DEFAULT '0',
  `id_script` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_rol`,`id_script`),
  KEY `rol_script_fk2_idx` (`id_script`),
  CONSTRAINT `rol_script_fk1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rol_script_fk2` FOREIGN KEY (`id_script`) REFERENCES `script` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_script`
--

LOCK TABLES `rol_script` WRITE;
/*!40000 ALTER TABLE `rol_script` DISABLE KEYS */;
INSERT INTO `rol_script` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16);
/*!40000 ALTER TABLE `rol_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script`
--

DROP TABLE IF EXISTS `script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_modulo` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL COMMENT 'Nombre de carpeta/Valor utilizado para cargar el modulo "dominio/admin/<url_modulo>/<url>"',
  `es_default` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `script_fk1_idx` (`id_modulo`),
  CONSTRAINT `script_fk1` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script`
--

LOCK TABLES `script` WRITE;
/*!40000 ALTER TABLE `script` DISABLE KEYS */;
INSERT INTO `script` VALUES (1,1,'listar',1),(2,1,'editar',0),(3,2,'listar',1),(4,2,'editar',0),(5,2,'ordenar',0),(6,3,'editar',1),(7,4,'listar',1),(8,4,'editar',0),(9,5,'editar',1),(10,6,'listar',1),(11,6,'editar',0),(12,7,'listar',1),(13,7,'editar',0),(14,8,'editar',1),(15,9,'listar',1),(16,9,'ver',0);
/*!40000 ALTER TABLE `script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_idioma`
--

DROP TABLE IF EXISTS `sys_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_idioma` (
  `id` varchar(2) NOT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_idioma`
--

LOCK TABLES `sys_idioma` WRITE;
/*!40000 ALTER TABLE `sys_idioma` DISABLE KEYS */;
INSERT INTO `sys_idioma` VALUES ('en','Inglés',NULL,1),('es','Español',NULL,1);
/*!40000 ALTER TABLE `sys_idioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_label`
--

DROP TABLE IF EXISTS `sys_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_label`
--

LOCK TABLES `sys_label` WRITE;
/*!40000 ALTER TABLE `sys_label` DISABLE KEYS */;
INSERT INTO `sys_label` VALUES (1),(2);
/*!40000 ALTER TABLE `sys_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_label_idioma`
--

DROP TABLE IF EXISTS `sys_label_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_label_idioma` (
  `id_label` int(11) NOT NULL,
  `id_idioma` varchar(25) NOT NULL,
  `traduccion` longtext,
  PRIMARY KEY (`id_label`,`id_idioma`),
  KEY `sys_label_idioma_fk2_idx` (`id_idioma`) USING BTREE,
  CONSTRAINT `sys_label_idioma_fk1` FOREIGN KEY (`id_label`) REFERENCES `sys_label` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sys_label_idioma_fk2` FOREIGN KEY (`id_idioma`) REFERENCES `sys_idioma` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_label_idioma`
--

LOCK TABLES `sys_label_idioma` WRITE;
/*!40000 ALTER TABLE `sys_label_idioma` DISABLE KEYS */;
INSERT INTO `sys_label_idioma` VALUES (1,'en','Male'),(1,'es','Masculino'),(2,'en','Female'),(2,'es','Femenino');
/*!40000 ALTER TABLE `sys_label_idioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabs`
--

DROP TABLE IF EXISTS `tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenido` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clase` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` text COLLATE utf8_unicode_ci,
  `destacado` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1_tabs_idx` (`contenido`),
  CONSTRAINT `fk1_tabs` FOREIGN KEY (`contenido`) REFERENCES `contenidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabs`
--

LOCK TABLES `tabs` WRITE;
/*!40000 ALTER TABLE `tabs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `urls`
--

DROP TABLE IF EXISTS `urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` tinytext COLLATE utf8_unicode_ci,
  `keywords` tinytext COLLATE utf8_unicode_ci,
  `follow` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1=>index,follow 2=>index,nofollow 3=>noindex,nofollow  4=>noindex,follow',
  `sitemap` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `urls`
--

LOCK TABLES `urls` WRITE;
/*!40000 ALTER TABLE `urls` DISABLE KEYS */;
/*!40000 ALTER TABLE `urls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valores`
--

DROP TABLE IF EXISTS `valores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valores` (
  `campo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` int(11) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`campo`,`contenido`,`valor`),
  KEY `fk2_valores_idx` (`contenido`),
  CONSTRAINT `fk1_valores` FOREIGN KEY (`campo`) REFERENCES `campos` (`alias`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk2_valores` FOREIGN KEY (`contenido`) REFERENCES `contenidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valores`
--

LOCK TABLES `valores` WRITE;
/*!40000 ALTER TABLE `valores` DISABLE KEYS */;
/*!40000 ALTER TABLE `valores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'starter_rest_api'
--

--
-- Dumping routines for database 'starter_rest_api'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-29 11:33:36
