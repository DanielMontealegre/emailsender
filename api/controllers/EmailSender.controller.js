//Variables
 var ValidatorHelper = require('../helpers/Validator.helper.js'),
    Config = require('../../config.js'),
    Constants = Config.Constants,
    LangConfig = Config.Lang,
	SESAWSService = require('../services/SESAWS.service.js'),
    MandrillService = require('../services/Mandrill.service.js'),
    SMTPService = require('../services/SMTP.service.js'),
	validator = require('validator'),
    Promise = require('bluebird');
	
var Errors, ErrorSystem, ErrorStrings, ErrorMessages, EmailLangConfig;

//function init
module.exports.init = function() {
    Errors = Config.Lang.errors[global.lang];
    ErrorSystem = Errors.system;
    ErrorStrings = Errors.strings;
    ErrorMessages = Errors.codes.Usuario; //cambiar
    EmailLangConfig = Config.Lang.email[global.lang];
}


/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */

module.exports.AppSendEmail = (req,res) => {

    req.body.tipo_servicio = req.headers.tipo_servicio;

	console.log("req.body.template es"+  req.body.template);

	var validate = ValidatorHelper.validate({
        tipo_servicio: ['not_empty'],
        to: ['not_empty'],
		from :  ['not_empty'],
		subject : ['not_empty'],
		template : ['not_empty']
    }, req.body);
    if (!validate.valido) {
        res.statusMessage = ErrorSystem["400"];
        res.status(400);
        res.json({
            valido: false,
            error: validate.error
        });
        return;
    }
	
	var emailService = req.body.tipo_servicio,
		to = req.body.to,
		from = req.body.from,
		subject = req.body.subject,
		body = req.body.template ;
		
	
	if( !(emailService == "ses" || emailService == "smtp" || emailService == "mandrill" ) ){
		res.statusMessage = ErrorSystem["400"];
        res.status(400);
        res.json({
            valido: false,
            error: "Servicio no valido!" //Cambiar por ErrorString o eso.
        });
        return;		
	}
	
	if ( !(from.hasOwnProperty("email") && from.hasOwnProperty("name") &&  validator.isEmail(from.email))){
		res.statusMessage = ErrorSystem["400"];
        res.status(400);
        res.json({
            valido: false,
		error: 'Formato invalido, parametro "from"   ' //Cambiar por ErrorString o eso.
        });
        return;			
	}
	
	if(!Array.isArray(to)){
		res.statusMessage = ErrorSystem["400"];
        res.status(400);
        res.json({
            valido: false,
            error: 'Formato invalido, parametro "to"(Debe ser un array)' //Cambiar por ErrorString o eso.
        });
        return;			
	}
	
	if(to.some( e =>  (!(e.hasOwnProperty("email") &&  e.hasOwnProperty("name") && validator.isEmail(e.email)) ))){
		res.statusMessage = ErrorSystem["400"];
        res.status(400);
        res.json({
            valido: false,
            error: 'Formato invalido, parametro "to"(Los objetos tienen el formato incorrecto)' //Cambiar por ErrorString o eso.
        });
        return;			
	}

	

	
	var ses = "ses",
		smtp = "smtp",
		mandrill ="mandrill";
	
	

		
    switch (emailService) {
        case ses :
            SESAWSService.sendMail(to, subject, body, from, function(result, error, params) {
				 handleCallback(req,res,result, error, params)
            });
            break;
        case mandrill :
            MandrillService.sendMail(to, subject, body, from, function(result, error, params) {
				handleCallback(req,res,result, error, params)
				});
            break;
        case smtp :
            SMTPService.sendMail(to, subject, body, from, function(result, error, params) {
               handleCallback(req,res,result, error, params)
			   });
            break;
        default:
            console.log('Servicio de envíos inválido');
            break;
    }

}

function handleCallback(req,res,result, error, params){
	     if (error) {
                    console.log(error);
						if (res.headersSent) return;
						res.statusMessage = ErrorSystem["500"];
						res.status(500);
						res.json({
                valido: false,
                error: {
                    mensaje: ErrorSystem["500"],
                    info: error
                }
            });
            return;
                } 
                    res.status(200)
					res.json({
						valido :true,
						resultado : result,
						params : params
					})
}
