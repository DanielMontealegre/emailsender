//arreglo general de la documentacion
var mode = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
var swagger = {
    swagger: "2.0",
    info: {
        title: 'Servicios de baumdigital',
        version: '0.4.8',
        description: 'RESTful Api | Services for baumdigital | Powered by NodeJS',
        termsOfService: "http://swagger.io/terms/",
        contact: {
            name: "Baum Digital 2017",
            email: "soporte@baumdigital.com",
            url: "https://soporte.baumdigital.com"
        },
        license: {
            name: "Apache 2.0",
            url: "http://www.apache.org/licenses/LICENSE-2.0.htm"
        }
    },
    host: process.env.NODE_URL_DOCS,
    basePath: '/',
    schemes: ['http', 'https'],
    definitions: require('./definitions.json'),
    securityDefinitions: require('./securityDefinitions.json'),
    tags: require('./tags.json'),
    consumes: [
        		 'application/json',
				  'application/x-www-form-urlencoded'
    ],
    produces: [
        'application/json'
    ],
    externalDocs: {
    }
};
module.exports = swagger;
