
var Config = require('../../config.js'),
        SMPTConfig = Config.Email.smtp_config,
        nodemailer = require('nodemailer');
/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = function (to, subject, body, from, callBack) {
    var smtp_options = {
        host: SMPTConfig.settings.host,
        port: SMPTConfig.settings.port,
        secure: true, // use SSL 
        auth: {
            user: SMPTConfig.settings.username,
            pass: SMPTConfig.settings.password
        }
    };

    var transporter = nodemailer.createTransport(smtp_options);

    var message_params = {
        from: emailObjectToString(from), // sender address 
        to: getEmailsString(to), // list of receivers 
        subject: subject, // Subject line 
        html: body // html body 
    };

    transporter.sendMail(message_params, function (error, result) {
        if (error) {
            callBack(null, error, message_params);
        } else {
            callBack(result);
        }
    });
};

function getEmailsString(objects) {
    var data = [];
    objects.forEach(function (object) {
        data.push(emailObjectToString(object));
    });
    return data.join(', ');
}

function emailObjectToString(object) {
    if (object.name && object.name !== '') {
        return '"' + object.name + '" <' + object.email + '>';
    } else {
        return object.email;
    }
}