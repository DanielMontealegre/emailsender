//Variables
var Config = require('../../config.js'),
        ConfigAWSSDK = Config.SDKs.sdk_aws_config,
        SESAWSConfig = Config.Email.ses_aws_config,
        AWS = require('aws-sdk');

//Configuracion
AWS.config.update(ConfigAWSSDK);
var SES = new AWS.SES({
    apiVersion: SESAWSConfig.version
});

/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = function (to, subject, body, from, callBack) {
    var message_params = {
        Destination: {ToAddresses: getEmailsString(to)},
        Message: {
            Body: {Html: {Data: body}},
            Subject: {Data: subject}
        },
        Source: emailObjectToString(from)
    };

    SES.sendEmail(message_params, function (error, result) {
        if (error) {
            callBack(null, error, message_params);
        } else {
            callBack(result);
        }
    });


};

function getEmailsString(objects) {
    var data = [];
    objects.forEach(function (object) {
        data.push(emailObjectToString(object));
    });
    return data;
}

/* Formato Nombre <Email> */
function emailObjectToString(object) {
    if (object.name && object.name !== '') {
        return '"' + object.name + '" <' + object.email + '>';
    } else {
        return object.email;
    }
}
