
var express = require('express'),
    AppRouter = express.Router(),
	ctrlEmailSender = require ('../controllers/EmailSender.controller.js'); 
	/*
	
	
	z		"template" : { "type": "string", "format": "binary"}
/*
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 
 
 	"type": "object",
	"properties": {
		"from" : { "$ref": "#/definitions/fromObject" },
		"to" : { "$ref": "#/definitions/toObject" },
		"subject" : { "type": "string", "format": "string"},
		"template" :  { "type": "string", "format": "string"}
	}
 
 
*/	
	 /**
 * @swagger
 *  email/send:
 *   post:
 *     tags:
 *       - EmailSender
 *     summary: Envia un correo.
 *     description: Envia un correo utilizando un servicio en especificio(ses, smtp o mandrill).
 *     operationId: email_sender
 *     parameters:
 *      - name: tipo_servicio
 *        in: header
 *        description: (ses , smtp, mandrill)
 *        required: true
 *        type: string
 *        format: string
 *      - name: body
 *        in: body
 *        description: Cuerpo del resquest
 *        required: true
 *        schema:
 *           $ref: '#/definitions/bodyObject'
 *     responses:
 *       200:
 *         description: El envio del email fue un exito.
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       400:
 *         description: Parámetros incorrectos o error de autentificacion
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post('/send',ctrlEmailSender.AppSendEmail);
	
	
	module.exports.App = AppRouter;