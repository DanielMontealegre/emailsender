//imports
var validator = require('validator'),
    Config = require('../../config.js'),
    Errors, ErrorSystem, ErrorStrings;

//function init
module.exports.init = function() {
    Errors = Config.Lang.errors[global.lang];
    ErrorSystem = Errors.system;
    ErrorStrings = Errors.strings;
}

/*
 *
 * @param {Array} params_require Vals: 'not_require', 'not_empty', 'email', 'number', 'decimal', 'boolean', 'fecha'
 * @param {Mixed} values
 * @returns {Array}
 */
module.exports.validate = function(params_require, params) {
    var validate = {
            valido: true
        },
        errores = {},
        errores_length = 0;
    for (var index in params_require) {
        if (!params_require.hasOwnProperty(index)) { // skip loop if the property is from prototype
            continue;
        }
        var conditions = params_require[index];
        if (params[index] !== undefined) {
            var element_validated = validateElement(conditions, params[index]);
            if (!element_validated.valido) {
                errores[index] = element_validated.error;
                errores_length++;
            }
        } else if (conditions.indexOf('not_require') === -1) {
            errores[index] = ErrorStrings.required;
            errores_length++;
        }
    }
    if (errores_length !== 0) {
        validate.valido = false;
        validate.error = {
            mensaje: ErrorStrings.params_error,
            parametros: errores
        };
    }
    return validate;
};

/*
 * @param {Array} conditions
 * @param {Mixed} value
 * @returns {Array(valido{, error})}
 */
function validateElement(conditions, param) {
    var param_validate = param + '';
    var validate_not_empty = false;
    if (conditions.indexOf('not_empty') !== -1) {
        validate_not_empty = true;
        if (param_validate === '') {
            return {
                valido: false,
                error: ErrorStrings.empty
            };
        }
    }

    if (conditions.indexOf('email') !== -1) {
        var email_valido = validator.isEmail(param_validate);
        if (!validate_not_empty && param_validate === '') {
            email_valido = true;
        }
        if (!email_valido) {
            return {
                valido: false,
                error: ErrorStrings.email
            };
        }
    } else if (conditions.indexOf('number') !== -1) {
        var numero_valido = validator.isNumeric(param_validate);
        if (!validate_not_empty && param_validate === '') {
            numero_valido = true;
        }
        if (!numero_valido) {
            return {
                valido: false,
                error: ErrorStrings.number
            };
        }
    } else if (conditions.indexOf('decimal') !== -1) {
        var float_valido = validator.isFloat(param_validate);
        if (!validate_not_empty && param_validate === '') {
            float_valido = true;
        }
        if (!float_valido) {
            return {
                valido: false,
                error: ErrorStrings.decimal
            };
        }
    } else if (conditions.indexOf('boolean') !== -1) {
        var boolean_valido = validator.isBoolean(param_validate);
        if (!validate_not_empty && param_validate === '') {
            boolean_valido = true;
        }
        if (!boolean_valido) {
            return {
                valido: false,
                error: ErrorStrings.boolean
            };
        }
    } else if (conditions.indexOf('fecha') !== -1) {
        var fecha_valida = validator.isDate(param_validate);
        if (!validate_not_empty && param_validate === '') {
            fecha_valida = true;
        } else {
            var r = /^((((19|[2-9]\d)\d{2})[-](0[13578]|1[02])[-](0[1-9]|[12]\d|3[01])\s(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|(((19|[2-9]\d)\d{2})[-](0[13456789]|1[012])[-](0[1-9]|[12]\d|30)\s(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|(((19|[2-9]\d)\d{2})[-](02)[-](0[1-9]|1\d|2[0-8])\s(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))[-](02)[-](29)\s(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])))$/g;
            if (!fecha_valida) {
                return {
                    valido: false,
                    error: ErrorStrings.date
                };
            } else if (!r.test(param_validate)) {
                return {
                    valido: false,
                    error: ErrorStrings.date_format
                };
            }
        }
    } else if (conditions.indexOf('solo_fecha') !== -1) {
        var fecha_valida = validator.isDate(param_validate);
        if (!validate_not_empty && param_validate === '') {
            fecha_valida = true;
        } else {
            var r = /^((((19|[2-9]\d)\d{2})[-](0[13578]|1[02])[-](0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})[-](0[13456789]|1[012])[-](0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})[-](02)[-](0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))[-](02)[-](29)))$/g;
            if (!fecha_valida) {
                return {
                    valido: false,
                    error: ErrorStrings.date_only
                };
            } else if (!r.test(param_validate)) {
                return {
                    valido: false,
                    error: ErrorStrings.date_only_format
                };
            }
        }
    }


    if (conditions.indexOf('greater_than_zero') !== -1) {
        if (param_validate <= 0) {
            return {
                valido: false,
                error: ErrorStrings.greater_than_zero
            };
        }
    }
    return {
        valido: true
    };
}
