//imports
var ValidatorHelper = require('./Validator.helper.js'),
    EmailSenderController = require ('../controllers/EmailSender.controller.js');
	

module.exports.initModules = function(req, res, next) {
    ValidatorHelper.init();
    EmailSenderController.init();
    next();
}
