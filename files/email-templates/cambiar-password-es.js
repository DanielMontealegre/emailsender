//composición del template
module.exports = {
    header: 'common/header.html',
    style: 'styles/default.css',
    top: 'common/top.html',
    content: 'content/cambiar-password-es.html',
    extra: [],
    footer: 'common/footer-es.html',
    font: "'Quattrocento Sans', sans-serif",
    title: "Starter - Titulo",
    footer_text: 'Texto del footer!',
    colors: {
        topBackground: '#2E3233',
        bodyBackground: '#FFFFFF',
        contentBackground: '#FFFFFF',
        tableContentBackground: '#EEEEEE',
        headersBackground: '#AE9345'
    }
};
