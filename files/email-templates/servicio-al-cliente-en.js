//composición del template
module.exports = {
    header: 'common/header.html',
    style: 'styles/default.css',
    top: 'common/top.html',
    content: 'content/servicio-al-cliente-en.html',
    extra: [],
    footer: 'common/footer-en.html',
    font: "'Quattrocento Sans', sans-serif",
    title: "Starter - Title",
    footer_text: 'Footer Text!',
    colors: {
        topBackground: '#2E3233',
        bodyBackground: '#FFFFFF',
        contentBackground: '#FFFFFF',
        tableContentBackground: '#EEEEEE',
        headersBackground: '#AE9345'
    }
};
