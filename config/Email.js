//las constantes
var Constants = require('../config/Constants.js');
/**
 *Configuración de Email
 */
module.exports = {
    current_email_service: 'ses_aws', //ses_aws, mandrill o smtp
    mandrill_config: {
        testing_key: '',
        production_key: ''
    },
    ses_aws_config: {
        version: '2010-12-01 ',
    },
    smtp_config: {
        settings: {
            host: 'smtp.gmail.com',
            port: 465,
            username: '',
            password: ''
        }
    }
};
