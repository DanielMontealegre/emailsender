/**
 *Configuración de las constantes del sistema
 */

module.exports = {
    port: 8081,
    url: process.env.NODE_URL,
    root: __dirname.replace(/\\/g, '/').replace('config', ''),
    url_admin: process.env.ADMIN_URL,
    url_images: process.env.IMAGES_URL
};